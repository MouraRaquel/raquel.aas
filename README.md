### Hi there 👋

## I'm Raquel Moura
[![Hits](https://hits.seeyoufarm.com/api/count/incr/badge.svg?url=https%3A%2F%2Fgithub.com%2FMouraRaquel&count_bg=%236D07C0&title_bg=%23000000&icon=&icon_color=%4716A3&title=Visualiza%C3%A7%C3%B5es&edge_flat=false)](https://github.com/MouraRaquel)
<br><br>



<div style="display: flex;" align="center">
  <a href="https://github.com/MouraRaquel">
  <img width="500em" height="270em" src="https://gitlab-readme-stats.vercel.app/api?username=MouraRaquel&show_icons=true&theme=buefy&include_all_commits=true&count_private=true&icon_color=aa88ff"/>
  <img width="440em" height="180em" src="https://github-readme-stats.vercel.app/api?username=MouraRaquel&bg_color=fff&title_color=41419f&text_color=000& include_all_commits=true&count_private=true&show_icons=true&icon_color=aa88ff"/>
  <img width="380em" height="180em" src="https://github-readme-stats.vercel.app/api/top-langs/?username=MouraRaquel&layout=compact&langs_count=7&title_color=41419f&text_color=000"/>
</div>
<div style="display: inline_block" align="center"><br>
  <img align="center" alt="HTML" height="40" width="50" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/html5/html5-original.svg">
  <img align="center" alt="CSS" height="40" width="50" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/css3/css3-original.svg">
  <img align="center" alt="rd-Python" height="40" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/python/python-original.svg">
  <img align="center" alt="Bootstrap" height="40" width="50" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/bootstrap/bootstrap-plain-wordmark.svg">
  <img align="center" alt="Js" height="40" width="50" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/javascript/javascript-plain.svg">
  <img align="center" alt="typescript" height="40" width="50" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/typescript/typescript-original.svg">
  <img align="center" alt="Angularjs" height="40" width="50" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/angularjs/angularjs-original.svg">
  <img align="center" alt="Nodejs" height="40" width="50" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/nodejs/nodejs-original.svg">
  <img align="center" alt="mysql" height="40" width="50" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/mysql/mysql-original.svg">
  <img align="center" alt="Php" height="40" width="50" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/php/php-original.svg">

</div>
  
##
<div align="center">
    
[![Linkedin](https://img.shields.io/badge/-Linkedin-0e76a8?style=for-the-badge&logo=Linkedin&logoColor=white)](https://www.linkedin.com/in/mouraraquel/)
[![Instagram](https://img.shields.io/badge/instagram-E4405F.svg?style=for-the-badge&logo=instagram&logoColor=white)](https://www.instagram.com/raquel.aasm/)
[![Facebook](https://img.shields.io/badge/facebook-005FED.svg?style=for-the-badge&logo=facebook&logoColor=white)](https://www.facebook.com/mouraraquel/)
[![GitHub](https://img.shields.io/badge/github-000.svg?style=for-the-badge&logo=github&logoColor=white)](https://github.com/MouraRaquel)
